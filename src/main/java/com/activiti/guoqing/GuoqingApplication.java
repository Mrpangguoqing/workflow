package com.activiti.guoqing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuoqingApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuoqingApplication.class, args);
	} 
}